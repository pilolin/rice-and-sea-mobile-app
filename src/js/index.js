import 'simplebar';
import {isTouchDevice} from './helper';
import tippy from 'tippy.js';
import { Swiper, Pagination } from 'swiper/swiper.esm.js';
import './modal';
import './catalog';
import './form';
import './tabs';
import './waves-effect';
import './../scss/styles.scss'

Swiper.use([Pagination]);

tippy('.catalog-item__info[data-tippy-content], .product-history__info[data-tippy-content]', {
	arrow: false,
	placement: 'bottom-end',
});

document.addEventListener('DOMContentLoaded', () => {
	if(isTouchDevice) {
		document.body.classList.add('touch');
	} else {
		document.body.classList.add('no-touch');
	}
});

window.addEventListener('load', () => {
	if (document.querySelector('.banner')) {
		new Swiper('.banner', {
			centeredSlides: true,
			pagination: {
				el: '.swiper-pagination',
				type: 'bullets',
				dynamicBullets: true
			},
		})
	}
});

document.addEventListener('click', (e) => {
	if (!e.target.closest('.dropdown__header')) return;

	const dropdown = e.target.closest('.dropdown');
	const dropdownContent = dropdown.querySelector('.dropdown__content');
	const isOpen = dropdown.getAttribute('aria-expanded') === 'true';
	const group = dropdown.dataset.group;

	// close all opened dropdowns with similar group
	if (group) {
		const groupOpenedDropdowns = document.querySelectorAll(`.dropdown[data-group="${group}"][aria-expanded="true"]`);

		for (const d of groupOpenedDropdowns) {
			d.setAttribute('aria-expanded', 'false');
			d.querySelector('.dropdown__content').style.height = '0px';
		}
	}

	if (isOpen) {
		dropdown.setAttribute('aria-expanded', 'false');
		dropdownContent.style.height = '0px';
	} else {
		dropdown.setAttribute('aria-expanded', 'true');
		dropdownContent.style.height = dropdownContent.scrollHeight + 'px';
	}

	e.preventDefault();
})

// add 'sticky' class tabs catalog when it's sticky
const catalogTabs = document.querySelector('.catalog-tabs');

if(catalogTabs) {
	window.addEventListener('scroll', () => {
		if (catalogTabs.offsetTop <= window.pageYOffset && !catalogTabs.classList.contains('is-sticky')) {
			catalogTabs.classList.add('is-sticky');
		} else if (catalogTabs.offsetTop > window.pageYOffset) {
			catalogTabs.classList.remove('is-sticky');
		}
	})
}