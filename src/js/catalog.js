import { Swiper } from 'swiper/swiper.esm.js';

window.addEventListener('load', () => {
	if (document.querySelector('.catalog-tabs')) {
		new Swiper('.catalog-tabs', {
			slidesPerView: 'auto',
			freeMode: true,
			freeModeSticky: true
		})
	}
});

// выбор таба в каталоге
document.addEventListener('click', (e) => {
	if(!e.target.closest('.catalog-tabs a')) return;

	document.querySelector('.catalog-tabs .swiper-slide[aria-selected="true"]').setAttribute('aria-selected', 'false');
	e.target.closest('.catalog-tabs .swiper-slide').setAttribute('aria-selected', 'true');
})

// выбор товара в каталоге
document.addEventListener('click', (e) => {
	if(!e.target.classList.contains('catalog-item__add-cart')) return;

	const addCartButton = e.target;
	const amountField = e.target.nextElementSibling;

	addCartButton.style.display = 'none';
	amountField.style.display = 'flex';

	e.stopPropagation();
	e.preventDefault();
});

// выбор допов на странице товара
document.addEventListener('click', (e) => {
	if(!e.target.classList.contains('spices__add-cart')) return;

	const addCartButton = e.target;
	const amountField = addCartButton.nextElementSibling;

	addCartButton.style.display = 'none';
	amountField.style.display = 'flex';

	e.stopPropagation();
	e.preventDefault();
});

// выбор товара на странице товара
document.addEventListener('click', (e) => {
	if(!e.target.classList.contains('product__btn-add-cart')) return;

	const addCartButton = e.target;
	const amountField = addCartButton.nextElementSibling;

	addCartButton.style.display = 'none';
	amountField.style.display = 'flex';

	e.stopPropagation();
	e.preventDefault();
});