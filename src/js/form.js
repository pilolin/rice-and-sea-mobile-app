import { Swiper } from 'swiper/swiper.esm.js';

// +/- in number field
document.addEventListener('click', (e) => {
	if(!e.target.closest('.form-item__type_number button')) return;

	const targetButton = e.target.closest('.form-item__type_number button');
	const field = e.target.closest('.form-item__type_number');
	const input = field.querySelector('input');
	
	if (targetButton.classList.contains('btn__add')) {
		input.value = +input.value + 1;
	} else {
		input.value = (+input.value > 1) ? +input.value - 1 : 1;
	}

	e.preventDefault();
});

// focus text field
document.addEventListener('focusin', (e) => {
	if ( !e.target.closest('.form-item__text-field input') ) return;

	e.target.closest('.form-item__text-field').classList.add('focus');
});

// change autofill text field
document.addEventListener('change', (e) => {
	if ( !e.target.closest('.form-item__text-field input') ) return;

	if ( !e.target.value.length ) return;

	const field = e.target.closest('.form-item__text-field');
	const inputButtonClear = field.querySelector('.clear');

	e.target.closest('.form-item__text-field').classList.add('focus');

	inputButtonClear.classList.add('active');
});

// blur text field
document.addEventListener('focusout', (e) => {
	if ( !e.target.closest('.form-item__text-field input') ) return;

	const field = e.target.closest('.form-item__text-field');
	const inputElement = e.target;
	const inputButtonClear = field.querySelector('.clear');

	if ( !inputElement.value.length ) {
		inputElement.parentElement.classList.remove('focus');

		inputButtonClear.classList.remove('active');
	}
});

// write in field text
document.addEventListener('input', (e) => {
	if ( !e.target.closest('.form-item__text-field input') ) return;

	const field = e.target.closest('.form-item__text-field');
	const inputElement = e.target;
	const inputButtonClear = field.querySelector('.clear');

	if ( inputElement.value.length && !inputButtonClear.classList.contains('active') ) {
		inputButtonClear.classList.add('active');
	} else if(!inputElement.value.length) {
		inputButtonClear.classList.remove('active');
	}
});

// clear field text
document.addEventListener('click', (e) => {
	if ( !e.target.closest('.form-item__text-field .clear') ) return;

	const field = e.target.closest('.form-item__text-field');
	const inputElement = field.querySelector('input');
	const inputButtonClear = e.target;
	
	field.classList.remove('focus');
	inputElement.value = '';
	inputButtonClear.classList.remove('active');
});

// focus textarea
document.addEventListener('focusin', (e) => {
	if ( !e.target.closest('.form-item__textarea textarea') ) return;

	e.target.closest('.form-item__textarea').classList.add('focus');
});

// change autofill textarea
document.addEventListener('change', (e) => {
	if ( !e.target.closest('.form-item__textarea input') ) return;

	if ( !e.target.value.length ) return;

	e.target.closest('.form-item__textarea').classList.add('focus');
});

// blur textarea
document.addEventListener('focusout', (e) => {
	if ( !e.target.closest('.form-item__textarea textarea') ) return;

	const textareaElement = e.target.closest('.form-item__textarea textarea');

	if ( !textareaElement.value.length ) {
		textareaElement.parentElement.classList.remove('focus');
	}
});

// color picker
window.addEventListener('load', () => {
	if (document.querySelectorAll('.form-item__color-picker')) {
		new Swiper('.form-item__color-picker .swiper-container', {
			slidesPerView: 'auto',
			freeMode: true,
			freeModeSticky: true,
			spaceBetween: 6
		});

		document.addEventListener('click', (e) => {
			if (!e.target.closest('.form-item__color-picker a')) return;

			const element = e.target.closest('.form-item__color-picker');
			const input = element.querySelector('input');
			const selectedColor = element.querySelector('a[aria-selected="true"]');
			const pickColor = e.target;

			input.value = pickColor.dataset.color;
			selectedColor.setAttribute('aria-selected', 'false');
			pickColor.setAttribute('aria-selected', 'true');

			e.preventDefault();
		})
	}
});