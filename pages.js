const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = [
	new HtmlWebpackPlugin({
		template: './src/loading.pug',
		filename: './loading.html'
	}),
	new HtmlWebpackPlugin({
		template: './src/index.pug',
		filename: './index.html'
	}),
	new HtmlWebpackPlugin({
		template: './src/product.pug',
		filename: './product.html'
	}),
	new HtmlWebpackPlugin({
		template: './src/select-city.pug',
		filename: './select-city.html'
	}),

	new HtmlWebpackPlugin({
		template: './src/profile.pug',
		filename: './profile.html'
	}),
	new HtmlWebpackPlugin({
		template: './src/settings.pug',
		filename: './settings.html'
	}),
	new HtmlWebpackPlugin({
		template: './src/settings-delivery.pug',
		filename: './settings-delivery.html'
	}),
	new HtmlWebpackPlugin({
		template: './src/delivery-list.pug',
		filename: './delivery-list.html'
	}),
	new HtmlWebpackPlugin({
		template: './src/history.pug',
		filename: './history.html'
	}),
	new HtmlWebpackPlugin({
		template: './src/bonus.pug',
		filename: './bonus.html'
	}),
	new HtmlWebpackPlugin({
		template: './src/sign-in.pug',
		filename: './sign-in.html'
	}),

	new HtmlWebpackPlugin({
		template: './src/contacts.pug',
		filename: './contacts.html'
	}),
	new HtmlWebpackPlugin({
		template: './src/delivery-zone.pug',
		filename: './delivery-zone.html'
	}),
	new HtmlWebpackPlugin({
		template: './src/delivery.pug',
		filename: './delivery.html'
	}),
	new HtmlWebpackPlugin({
		template: './src/stocks.pug',
		filename: './stocks.html'
	}),
	new HtmlWebpackPlugin({
		template: './src/bonus-system.pug',
		filename: './bonus-system.html'
	}),
	new HtmlWebpackPlugin({
		template: './src/about.pug',
		filename: './about.html'
	}),
	new HtmlWebpackPlugin({
		template: './src/pay-and-return.pug',
		filename: './pay-and-return.html'
	}),
	new HtmlWebpackPlugin({
		template: './src/map.pug',
		filename: './map.html'
	}),

	new HtmlWebpackPlugin({
		template: './src/basket.pug',
		filename: './basket.html'
	}),
	new HtmlWebpackPlugin({
		template: './src/empty-basket.pug',
		filename: './empty-basket.html'
	}),
	new HtmlWebpackPlugin({
		template: './src/checkout.pug',
		filename: './checkout.html'
	}),
	new HtmlWebpackPlugin({
		template: './src/order-final.pug',
		filename: './order-final.html'
	}),

	new HtmlWebpackPlugin({
		template: './src/all-modals.pug',
		filename: './all-modals.html'
	}),
]